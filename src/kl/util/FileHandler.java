package kl.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import java.text.SimpleDateFormat;
import java.util.BitSet;
import java.util.Date;
import java.util.HashSet;

import kl.feature.Feature;
import kl.feature.BinaryFeature;

public class FileHandler {
	public static Collection<Feature> readNetwork(String file) {
		java.io.BufferedReader br = null;

		HashSet<Feature> fs = new HashSet<Feature>();

		try {
			String sCurrentLine;

			br = new java.io.BufferedReader(new java.io.FileReader(file));

			// read cardinalities
			sCurrentLine = br.readLine();
			String[] cards = sCurrentLine.split(",");
			int[] domain = new int[cards.length];
			for (int i = 0; i < cards.length; i++)
				domain[i] = Integer.parseInt(cards[i]);

			while ((sCurrentLine = br.readLine()) != null) {
				sCurrentLine = sCurrentLine.trim();


				if (sCurrentLine.matches("MN \\{") ||
				    sCurrentLine.matches("\\}") ||
				    sCurrentLine.matches("\n") ||
				    sCurrentLine.matches("BN \\{") ||
				    sCurrentLine.matches("v[0-9]+ \\{") ||
				    sCurrentLine.matches("table \\{")) continue;

				BinaryFeature f = new BinaryFeature(domain.length);

				String[] values = sCurrentLine.split(" ");
				BitSet vars = new BitSet(cards.length);
				int[] vals = new int[values.length - 1];
				for (int i = 1; i < values.length; i++) {
					values[i] = values[i].replace("+v", "");
					String[] feature = values[i].split("_");

					f.addVal(Integer.parseInt(feature[0]),
						 Integer.parseInt(feature[1]));
					f.setWeight(Double.parseDouble(values[0]));

				}

				fs.add(f);
			}
		} catch (java.io.IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (java.io.IOException ex) {
				ex.printStackTrace();
			}
		}

		return(fs);
        }

    public static int readNumberOfVariables(String file) {
        int number=0;
        java.io.BufferedReader br = null;

            HashSet<Feature> fs = new HashSet<Feature>();

            try {
                String sCurrentLine;

                br = new java.io.BufferedReader(new java.io.FileReader(file));

                // read cardinalities
                sCurrentLine = br.readLine();
                String[] cards = sCurrentLine.split(",");
                number = cards.length;

            } catch (java.io.IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (br != null)br.close();
                } catch (java.io.IOException ex) {
                    ex.printStackTrace();
                }
            }
            return number;

    }

}