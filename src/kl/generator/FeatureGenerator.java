package kl.generator;

import java.util.Collection;
import java.util.List;
import java.util.LinkedHashSet;

import kl.feature.Feature;
import kl.feature.BinaryFeature;


public class FeatureGenerator {
	public static Collection<Feature>
	generateFromDomain(List<Integer> card) {
		int[] s = new int[card.size()];
		Collection<Feature> features = new LinkedHashSet<Feature>();
		Feature feature;
		int nAssig = 1;

		for (Integer numberOfValues : card) nAssig *= numberOfValues;

		// loop over all assigment
		for (int i = 0; i < nAssig; i++) {
			for (int l = 0; l < s.length; l++)
				if (++s[l] == card.get(l))
					s[l] = 0;
				else break;

			feature = new BinaryFeature(card.size());

			for (int l = 0; l < s.length; l++)
				feature.addVal(l, s[l]);

			features.add(feature);
		}

		return features;
	}
}