package kl.feature;

import java.util.BitSet;


/**
 * A binary feature is represented as an array of boolean.
 *
 **/
public class BinaryFeature extends Feature {
	private final BitSet _val;

	public BinaryFeature(int n) {
		super(n);
		this._val = new BitSet(n);
	}

        public BinaryFeature(BinaryFeature f) {
	        super(f.getNumberOfDomainVariables(), f.getVar());
		this._val = new BitSet(f.getNumberOfDomainVariables());

		for(int i = f._val.nextSetBit(0); i >= 0; i = f._val.nextSetBit(i+1))
			this.addVal(i, 1);
        }

        @Override
	public boolean isSatisfied(Feature otherFeature) {
		for(int i = this.getVar().nextSetBit(0); i >= 0; i = this.getVar().nextSetBit(i+1)) {
			if(!otherFeature.getVar().get(i)) return false;

			if (this.getVal(i) != otherFeature.getVal(i)) return false;
		}

		return true;
	}


        @Override
	public void addVal(int var, Integer val) {
	        if (val == null) {
			this._val.set(var, false);
			this.rmVar(var);
			return;
		}

		this.addVar(var);

		if (val == 0) this._val.set(var, false);
		else this._val.set(var, true);
	}

        @Override
	public Integer getVal(int var) {
	        return this._val.get(var) ? 1 : 0;
	}


        @Override
	public int getLength() {
		return this.getVar().cardinality();
	}

        @Override
	public BinaryFeature clone() {
		return new BinaryFeature(this);
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = 23 * hash + (this._val == null ? 0 : this._val.hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final BinaryFeature f = (BinaryFeature) obj;

		return this.getVar().equals(f.getVar()) && this._val.equals(f._val);
	}


        @Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		for(int i=this.getVar().nextSetBit(0); i>=0; i=this.getVar().nextSetBit(i+1)) {
			stringBuilder.append("+v" + i + "_" + getVal(i) + " ");
		}

		return stringBuilder.toString().trim();
	}
}
