package kl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import kl.util.FileHandler;
import kl.feature.Feature;
import kl.generator.FeatureGenerator;


/**
 *
 * @author yane
 */
public class KLdivergence {
	private static Collection<Feature> _assignments;

	public static void main(String[] args) {
		double result = KLdivergence.KL(args[0], args[1]);
		System.out.println(result);
	}

	public static double KL(String underlying, String learned){
		double KL = 0;

		int numberOfVariables = FileHandler.readNumberOfVariables(underlying);

		// Define Cardinalities
		ArrayList<Integer> cards = new ArrayList<Integer>();
		for(int i=0;i<numberOfVariables;i++) cards.add(2);

		// Define assignments to compute KL divergence
		_assignments = FeatureGenerator.generateFromDomain(cards);

		// Read both networks from .mn files
		Collection<Feature> P = FileHandler.readNetwork(underlying);
		Collection<Feature> Q = FileHandler.readNetwork(learned);

		double[] partitionFunctions = computeZ(P, Q);
		double Z_p = partitionFunctions[0];
		double Z_q = partitionFunctions[1];

		double p, q;
		for(Feature x : _assignments){
			// unnormalized distributions
			p = Math.exp(computeProb(P, x));
			q = Math.exp(computeProb(Q, x));

			// p and q are Gibbs distributions. Formally:
			//
			// p*(X) = 1/Z_p \prod_i \phi_i
			// p*(X) = 1/Z_p \exp { \sum_j \theta_j }
			//
			// where p(x) is an unnormalized distribution p(X)=\exp{\sum_j\theta_j}
			//
			// Then, the KL divergence is defined as follows:
			//
			// KL = \sum_x p*(x) log (p*(x)/q*(x))
			// KL = 1/Z_p \sum_x exp(p(x)) * log((p(x)/Z_p)/(q(x)/Z_q))
			//

			KL += p * Math.log((p/Z_p) / (q/Z_q));
		}

		KL = KL / Z_p;

		return KL;
	}

	// P is a set of features (of a log-linear model)
	// x is a complete assignment of the domain variables
	public static double computeProb(Collection<Feature> P, Feature x) {
		double p_x = 0.0;
		for(Feature f : P)
			if (f.isSatisfied(x)) // Is the feature activated with the assignment x?
				p_x += f.getWeight();

		return p_x;
	}

	// P and Q are a set of features (of a log-linear model)
	public static double[] computeZ(Collection<Feature> P, Collection<Feature> Q) {
		double[] Z = new double[2];

		for(Feature x : _assignments) {
			Z[0] += Math.exp(computeProb(P, x));
			Z[1] += Math.exp(computeProb(Q, x));
		}

		return Z;
	}

}
