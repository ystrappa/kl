package kl.feature;

import java.util.Arrays;
import java.util.BitSet;

/**
 * This abstract class represents a feature, which is a conjunction of
 * variable assignments.
 */
public abstract class Feature {
    private final BitSet _var;
    private final int _numberOfDomainVariables;
    private Double weight;

    public Double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    public Feature(int n) {
	this._numberOfDomainVariables = n;
	this._var = new BitSet(n);
    }

    protected Feature(int n, BitSet var) {
	this._numberOfDomainVariables = n;
	this._var = (BitSet)var.clone();
    }

    public BitSet getVar() {
	return this._var;
    }

    public void addVar(int var) {
	this._var.set(var);
    }

    public void rmVar(int var) {
	this._var.clear(var);
    }

    protected int getNumberOfDomainVariables() {
	return this._numberOfDomainVariables;
    }

    public abstract boolean isSatisfied(Feature otherFeature);

    public abstract void addVal(int var, Integer val);

    public abstract Integer getVal(int var);

    public abstract int getLength();

    public abstract Feature clone();

    public abstract boolean equals(Object object);

    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this._var.hashCode();

        return hash;
    }
}